/*
 * proj.h
 *
 *  Created on: Aug 22, 2023
 *      Author: Admin
 */
#include<stdio.h>
#ifndef INC_PROJ_H_
#define INC_PROJ_H_

typedef struct logs
{
	char id[64];
	char name[64];
	char type[32];
	char inout[8];
}log;

typedef struct users
{
	char rfid[64];
	char pwd[64];
	struct logs l1;
}user;

char Read_keypad();
#endif /* INC_PROJ_H_ */
